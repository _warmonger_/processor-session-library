package com.bitbucket._warmonger_.processorSession;

import java.util.HashMap;

import com.bitbucket._warmonger_.processorSession.site.ISite;
import com.bitbucket._warmonger_.processorSession.site.ISiteSession;

public class SessionMap<TKey> extends HashMap<TKey, SessionMap.BoundedPair<?, ?>>
{
	public static class BoundedPair<TSite extends ISite<?, ? super TSession>, TSession extends ISiteSession>
	{
		private TSite site;
		private TSession session;
		
		public BoundedPair(TSite site, TSession session)
		{
			this.site = site;
			this.session = session;
		}
		
		public TSite GetSite()
		{
			return site;
		}
		
		public boolean SetSite(TSite site)
		{
			this.site = site;
			
			return true;
		}
		
		public TSession GetSession()
		{
			return session;
		}
		
		public boolean SetSession(TSession session)
		{
			this.session = session;
			
			return true;
		}
	}
	
	private static final long serialVersionUID = -5222169446254296808L;
}
