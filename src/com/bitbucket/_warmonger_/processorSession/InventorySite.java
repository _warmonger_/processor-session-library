package com.bitbucket._warmonger_.processorSession;

import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryCreativeEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.bitbucket._warmonger_.processorSession.data.ConstInventoryDataContainer;
import com.bitbucket._warmonger_.processorSession.data.InventoryDataContainer;
import com.bitbucket._warmonger_.processorSession.site.IInvSite;
import com.bitbucket._warmonger_.processorSession.site.ISite;
import com.bitbucket._warmonger_.processorSession.site.ISiteSession;
import com.bitbucket._warmonger_.processorSession.struct.ISessionFactory;

public class InventorySite<TGlobParam extends InventoryDataContainer<? extends ConstInventoryDataContainer>, TSession extends InventorySiteSession>
		extends Site<TGlobParam, TSession> implements IInvSite<TGlobParam, TSession>
{
	String defaultTitle;
	protected ItemStack[] staticContent;

	public InventorySite(ISessionFactory<? super TGlobParam, ? extends TSession> sessionFactory)
	{
		this("", new ItemStack[54], sessionFactory);
	}

	public InventorySite(String defaultTitle, ItemStack[] staticContent,
			ISessionFactory<? super TGlobParam, ? extends TSession> sessionFactory)
	{
		super(sessionFactory);

		this.defaultTitle = defaultTitle;
		this.staticContent = staticContent;
	}

	@Override
	public String GetTitle(TGlobParam param, TSession session)
	{
		return defaultTitle;
	}

	@Override
	public int GetContainerSize(TSession siteSession)
	{
		return (((staticContent.length - 1) / 9) + 1) * 9;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public boolean Update(TGlobParam param, TSession session)
	{
		if (session.HasActiveSubSiteSession()) { return ((ISite) GetSubSite(session.GetActiveSubSiteSessionID()))
				.Update(param, GetActiveSubSiteSession(session, param)); }

		Inventory inv = param.GetInventory();

		/*
		 * if(inv.getSize() != GetContainerSize(session)) { Inventory newInv =
		 * Bukkit.createInventory(null, GetContainerSize(session));
		 * param.GetDataPackage().SetInventory(newInv);
		 * 
		 * inv.clear(); }
		 */

		inv.setContents(GetContent(session));

		InventoryTitleHelper.SetTitle(inv, this.GetTitle(param, session));

		return true;
	}

	@Override
	public ItemStack[] GetContent(TSession siteSession)
	{
		ItemStack[] res = new ItemStack[staticContent.length];

		for (int i = 0; i < res.length; i++)
		{
			res[i] = staticContent[i] == null ? null : staticContent[i].clone();
		}

		return res;
	}
	
	@Override
	public boolean ProcessThis(ISiteSession caller, TGlobParam extData, TSession session)
	{
		Event event = extData.GetEvent();
		
		boolean result = false;
		
		if (event instanceof InventoryClickEvent)
		{
			result |= OnInventoryClick((InventoryClickEvent) event, caller, extData, session);
			
			if (event instanceof InventoryCreativeEvent)
			{
				result |= OnInventoryCreative((InventoryCreativeEvent) event, caller, extData, session);
			}
		}
		else if (event instanceof InventoryInteractEvent)
		{
			result |= OnInventoryInteract((InventoryInteractEvent) event, caller, extData, session);
			
			if (event instanceof InventoryDragEvent)
			{
				result |= OnInventoryDrag((InventoryDragEvent) event, caller, extData, session);
			}
		}
		else if (event instanceof InventoryCloseEvent)
		{
			return OnInventoryClose((InventoryCloseEvent) event, caller, extData, session);
		}
		else
		{
			return false;
		}

		return result;
	}

	@Override
	public boolean OnInventoryClick(InventoryClickEvent ev, ISiteSession caller, TGlobParam extData, TSession siteSession)
	{
		return false;
	}

	@Override
	public boolean OnInventoryClose(InventoryCloseEvent ev, ISiteSession caller, TGlobParam extData, TSession siteSession)
	{
		return false;
		//Dispose();
	}

	@Override
	public boolean OnInventoryDrag(InventoryDragEvent ev, ISiteSession caller, TGlobParam extData, TSession siteSession)
	{
		return false;
	}

	@Override
	public boolean OnInventoryInteract(InventoryInteractEvent ev, ISiteSession caller, TGlobParam extData,
			TSession siteSession)
	{
		return false;
	}

	@Override
	public boolean OnInventoryCreative(InventoryCreativeEvent ev, ISiteSession caller, TGlobParam extData,
			TSession siteSession)
	{
		return false;
	}
}
