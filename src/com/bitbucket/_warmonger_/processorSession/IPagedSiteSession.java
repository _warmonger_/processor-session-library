package com.bitbucket._warmonger_.processorSession;

import com.bitbucket._warmonger_.processorSession.site.IInvSiteSession;

public interface IPagedSiteSession extends IInvSiteSession
{
	public int GetCurrentPageID();
}
