package com.bitbucket._warmonger_.processorSession;

public class InventoryPagedSiteSession extends InventorySiteSession implements IPagedSiteSession
{
	int currentPageID;
	
	@Override
	public int GetCurrentPageID()
	{
		return currentPageID;
	}
}
