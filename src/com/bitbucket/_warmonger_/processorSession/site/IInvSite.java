package com.bitbucket._warmonger_.processorSession.site;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryCreativeEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.ItemStack;

public interface IInvSite<TGlobParam, TSession extends IInvSiteSession> extends ISite<TGlobParam, TSession>
{
	public String GetTitle(TGlobParam param, TSession siteSession);
	
	public int GetContainerSize(TSession siteSession);
	
	public ItemStack[] GetContent(TSession siteSession);
	
	public boolean OnInventoryClick(InventoryClickEvent ev, ISiteSession caller, TGlobParam extData, TSession siteSession); // Handles inventory click

	public boolean OnInventoryClose(InventoryCloseEvent ev, ISiteSession caller, TGlobParam extData, TSession siteSession);
	
	public boolean OnInventoryDrag(InventoryDragEvent ev, ISiteSession caller, TGlobParam extData, TSession siteSession);
	
	public boolean OnInventoryInteract(InventoryInteractEvent ev, ISiteSession caller, TGlobParam extData, TSession siteSession);
	
	public boolean OnInventoryCreative(InventoryCreativeEvent ev, ISiteSession caller, TGlobParam extData, TSession siteSession);
}
