package com.bitbucket._warmonger_.processorSession.site;

import com.bitbucket._warmonger_.processorSession.struct.ISession;

public interface ISiteSession extends ISession
{
	public int GetActiveSubSiteSessionID();
	public boolean SetActiveSubSiteSessionID(int ID);
	public boolean UnsetActiveSubSiteSession();
	
	public boolean HasActiveSubSiteSession();
	public ISiteSession GetActiveSubSiteSession();	
	
	public ISiteSession GetSubSiteSession(int ID);
	public boolean HasSubSiteSession(int ID);
	public boolean SetSubSiteSession(int ID, ISiteSession session);
}
