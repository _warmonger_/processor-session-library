package com.bitbucket._warmonger_.processorSession.site;

import com.bitbucket._warmonger_.processorSession.struct.ICascadeProcessor;
import com.bitbucket._warmonger_.processorSession.struct.ISessionFactory;

public interface ISite<TGlobParam, TSession extends ISiteSession> extends ICascadeProcessor<ISiteSession, TGlobParam, TSession, Boolean>, ISessionFactory<TGlobParam, TSession>
{
	public boolean HasSubSite(Integer ID);
	public ISite<? super TGlobParam, ?> GetSubSite(Integer ID);
	
	public boolean Update(TGlobParam param, TSession session);
}
