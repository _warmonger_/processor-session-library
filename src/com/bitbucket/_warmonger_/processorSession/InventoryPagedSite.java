package com.bitbucket._warmonger_.processorSession;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import com.bitbucket._warmonger_.processorSession.data.ConstInventoryDataContainer;
import com.bitbucket._warmonger_.processorSession.data.InventoryDataContainer;
import com.bitbucket._warmonger_.processorSession.site.ISiteSession;
import com.bitbucket._warmonger_.processorSession.struct.ISessionFactory;

public abstract class InventoryPagedSite<TParam extends InventoryDataContainer<? extends ConstInventoryDataContainer>, TSession extends InventoryPagedSiteSession> extends InventorySite<TParam, TSession> implements IPagedSite<TParam, ItemStack, TSession>
{	
	public InventoryPagedSite(String title, ItemStack[] content, ISessionFactory<? super TParam, ? extends TSession> sessionFactory)
	{
		super(title, content, sessionFactory);
	}
	
	public int GetElementIndex(InventoryClickEvent ev, TSession siteSession, TParam param)
	{
		return GetElementIndex(ev.getRawSlot(), siteSession, param);
	}
	
	public int GetElementIndex(int invSlot, TSession siteSession, TParam param)
	{
		Integer[] elementMap = this.GetElementMap(siteSession);
		
		for(int i = 0; i < elementMap.length; i++)
		{
			if(elementMap[i] == invSlot)
			{
				int index = i + siteSession.GetCurrentPageID() * this.GetElementsPerPage(siteSession);
				
				return index < GetElementsAmount(siteSession) ? index : -1;
			}
		}
		
		return -1;
	}
	
	public boolean NextPage(TSession siteSession)
	{
		if ((siteSession.currentPageID + 1) * GetElementMap(siteSession).length < GetElementsAmount(siteSession))
		{
			siteSession.currentPageID++;
			
			//param.GetConstantData().get.Update();

			return true;
		}

		return false;
	}

	public boolean PreviousPage(TSession siteSession)
	{
		if (siteSession.currentPageID > 0)
		{
			siteSession.currentPageID--;
			
			if(true)//siteSession.GetDataPackage().GetGlobalSession().Update())
			{
				return true;
			}
			else
			{
				siteSession.currentPageID++;
				
				return false;
			}
		}

		return false;
	}
	
	@Override
	public boolean OnInventoryClick(InventoryClickEvent ev, ISiteSession caller, TParam extData, TSession siteSession)
	{
		int elementIndex = GetElementIndex(ev.getRawSlot(), siteSession, extData);
		
		if(elementIndex > -1)
		{
			return OnElementClick(elementIndex, caller, extData, siteSession);
		}
		
		return false;
	}
	
	public abstract boolean OnElementClick(int clickedElementIndex, ISiteSession caller, TParam extData, TSession siteSession);
}
