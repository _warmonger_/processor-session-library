package com.bitbucket._warmonger_.processorSession;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.bitbucket._warmonger_.processorSession.site.ISiteSession;

public class SiteSession implements ISiteSession
{
	Map<Integer, ISiteSession> subSiteSessions = new HashMap<Integer, ISiteSession>();
	Integer activeSubSiteSessionID = -1;

	@Override
	public void Dispose()
	{
		for (Entry<Integer, ISiteSession> en : subSiteSessions.entrySet())
		{
			if (!en.getValue().IsDisposed())
			{
				en.getValue().Dispose();
			}
		}

		subSiteSessions = null;
	}

	@Override
	public boolean IsDisposed()
	{
		return subSiteSessions == null;
	}

	@Override
	public int GetActiveSubSiteSessionID()
	{
		return activeSubSiteSessionID;
	}

	@Override
	public boolean SetActiveSubSiteSessionID(int ID)
	{
		activeSubSiteSessionID = ID;

		return true;
	}

	@Override
	public boolean HasActiveSubSiteSession()
	{
		return activeSubSiteSessionID >= 0;
	}

	@Override
	public ISiteSession GetActiveSubSiteSession()
	{
		if (HasActiveSubSiteSession())
		{
			if (subSiteSessions
					.containsKey(activeSubSiteSessionID)) { return subSiteSessions.get(activeSubSiteSessionID); }
		}

		return null;
	}

	@Override
	public ISiteSession GetSubSiteSession(int ID)
	{
		if (subSiteSessions.containsKey(activeSubSiteSessionID)) { return subSiteSessions.get(activeSubSiteSessionID); }

		return null;
	}

	@Override
	public boolean HasSubSiteSession(int ID)
	{
		return subSiteSessions.containsKey(activeSubSiteSessionID);
	}

	@Override
	public boolean SetSubSiteSession(int ID, ISiteSession session)
	{		
		subSiteSessions.put(ID, session);

		return true;
	}

	@Override
	public boolean UnsetActiveSubSiteSession()
	{
		activeSubSiteSessionID = -1;
		
		return true;
	}
}
