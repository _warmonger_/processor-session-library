package com.bitbucket._warmonger_.processorSession.exceptions;

import com.bitbucket._warmonger_.processorSession.site.ISite;
import com.bitbucket._warmonger_.processorSession.site.ISiteSession;

/** Thrown when there is no existing subSite with given ID **/
public class NoSuchSiteException extends RuntimeException
{
	private static final long serialVersionUID = -4334085529849084695L;
	
	ISite<?, ?> site;
	ISiteSession siteSession;
	
	int missingSubSiteID;
	
	public NoSuchSiteException(ISite<?, ?> site,ISiteSession siteSession,int missingSubSiteID)
	{
		this.site = site;
		this.siteSession = siteSession;
		
		this.missingSubSiteID = missingSubSiteID;
	}
}
