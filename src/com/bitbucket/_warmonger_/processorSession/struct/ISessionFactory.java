package com.bitbucket._warmonger_.processorSession.struct;

public interface ISessionFactory<TParam, TSession extends ISession>
{
	public TSession CreateSession(TParam parameter);
}
