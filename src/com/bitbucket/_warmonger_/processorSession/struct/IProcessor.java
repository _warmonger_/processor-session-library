package com.bitbucket._warmonger_.processorSession.struct;

public interface IProcessor<TCaller, TData, TOutput>
{
	public TOutput Process(TCaller caller, TData session);
}