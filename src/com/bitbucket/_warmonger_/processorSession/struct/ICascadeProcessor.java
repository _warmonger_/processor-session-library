package com.bitbucket._warmonger_.processorSession.struct;

import org.apache.commons.lang3.tuple.Pair;

public interface ICascadeProcessor<TCaller, TExtData, TData, TOutput> extends IProcessor<TCaller, Pair<TExtData, TData>, TOutput>
{
	@Override
	public default TOutput Process(TCaller caller, Pair<TExtData, TData> data)
	{
		return Process(caller, data.getLeft(), data.getRight());
	}
	
	public TOutput Process(TCaller caller, TExtData extData, TData data);
}
