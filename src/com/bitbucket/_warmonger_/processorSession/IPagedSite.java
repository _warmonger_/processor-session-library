package com.bitbucket._warmonger_.processorSession;

import com.bitbucket._warmonger_.processorSession.site.ISite;
import com.bitbucket._warmonger_.processorSession.site.ISiteSession;

public interface IPagedSite<TGlobParam, TElement, TSession extends ISiteSession> extends ISite<TGlobParam, TSession>
{
	public TElement[] GetElements(TSession siteSession);
	
	public TElement GetElement(TSession siteSession, int index);
	
	public int GetElementsAmount(TSession siteSession);
	
	public int GetElementsPerPage(TSession siteSession);
	
	public Integer[] GetElementMap(TSession siteSession);
	
	public boolean NextPage(TSession siteSession);

	public boolean PreviousPage(TSession siteSession);
}
