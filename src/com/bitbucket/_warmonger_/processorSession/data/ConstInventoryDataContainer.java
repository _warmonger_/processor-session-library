package com.bitbucket._warmonger_.processorSession.data;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.inventory.Inventory;

import rtwarmonger.global.IDisposable;

public class ConstInventoryDataContainer implements IInventoryOperandProvider, IDisposable
{
	Inventory inventory;
	UUID owner;
	
	Map<String, Object> extremelyExternalData = new HashMap<>();
	
	boolean isDisposed;
	
	public ConstInventoryDataContainer(Inventory inventory, UUID owner)
	{
		this.inventory = inventory;
		this.owner = owner;
	}
	
	@Override
	public Inventory GetInventory()
	{
		return inventory;
	}

	@Override
	public boolean SetInventory(Inventory inv)
	{
		this.inventory = inv;
		
		return true;
	}

	@Override
	public UUID GetOwner()
	{
		return owner;
	}

	@Override
	public boolean SetOwner(UUID owner)
	{
		this.owner = owner;
		
		return true;
	}
	
	public Map<String, Object> GetExternalDataMap()
	{
		return extremelyExternalData;
	}
	
	public Object GetExternalData(String key)
	{
		return extremelyExternalData.get(key);
	}
	
	public Object SetExternalData(String key, Object newObj)
	{
		return extremelyExternalData.put(key, newObj);
	}

	@Override
	public void Dispose()
	{
		extremelyExternalData.clear();
		inventory.clear();
		
		owner = null;
		
		isDisposed = true;
	}

	@Override
	public boolean IsDisposed()
	{
		return isDisposed;
	}
}
