package com.bitbucket._warmonger_.processorSession.data;

import java.util.UUID;

import org.bukkit.inventory.Inventory;

public interface IInventoryOperandProvider
{
	public Inventory GetInventory();
	public boolean SetInventory(Inventory inv);	
	
	public UUID GetOwner();	
	public boolean SetOwner(UUID owner);
}
