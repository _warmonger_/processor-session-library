package com.bitbucket._warmonger_.processorSession.data;

import java.util.UUID;

import org.bukkit.event.Event;
import org.bukkit.inventory.Inventory;

import rtwarmonger.global.IDisposable;

public class InventoryDataContainer<TCIDC extends ConstInventoryDataContainer> implements IInventoryOperandProvider, IDisposable
{
	// CONSTANT DATA
	TCIDC constData;

	// VARIABLE DATA
	Event event;

	boolean isDisposed = false;

	public InventoryDataContainer(TCIDC constData, Event event)
	{
		this.constData = constData;
		this.event = event;
	}

	public TCIDC GetConstantData()
	{
		return constData;
	}

	public Event GetEvent()
	{
		return event;
	}

	public Inventory GetInventory()
	{
		return constData.GetInventory();
	}
	
	@Override
	public boolean SetInventory(Inventory inv)
	{
		return constData.SetInventory(inv);
	}

	@Override
	public UUID GetOwner()
	{
		return constData.GetOwner();
	}

	@Override
	public boolean SetOwner(UUID owner)
	{
		return constData.SetOwner(owner);
	}

	@Override
	public void Dispose()
	{
		if (!constData.IsDisposed())
		{
			constData.Dispose();
		}
		
		event = null;
	}

	@Override
	public boolean IsDisposed()
	{
		return constData == null || constData.isDisposed;
	}
}
