package com.bitbucket._warmonger_.processorSession;

public interface DataProvider<TInput, TOutput>
{
	public TOutput Receive(TInput input);
}
