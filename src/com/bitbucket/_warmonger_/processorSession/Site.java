package com.bitbucket._warmonger_.processorSession;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;

import com.bitbucket._warmonger_.processorSession.data.ConstInventoryDataContainer;
import com.bitbucket._warmonger_.processorSession.data.InventoryDataContainer;
import com.bitbucket._warmonger_.processorSession.exceptions.NoSuchSiteException;
import com.bitbucket._warmonger_.processorSession.site.ISite;
import com.bitbucket._warmonger_.processorSession.site.ISiteSession;
import com.bitbucket._warmonger_.processorSession.struct.ISessionFactory;

public abstract class Site<TGlobParam extends InventoryDataContainer<? extends ConstInventoryDataContainer>, TSession extends SiteSession> implements ISite<TGlobParam, TSession>
{
	protected Map<Integer, ISite<? super TGlobParam, ?>> subSites = new HashMap<>();
	ISessionFactory<? super TGlobParam, ? extends TSession> sessionFactory;
	
	public Site(ISessionFactory<? super TGlobParam, ? extends TSession> sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public boolean HasSubSite(Integer ID)
	{
		return subSites.containsKey(ID);
	}

	@Override
	public ISite<? super TGlobParam, ?> GetSubSite(Integer ID)
	{
		if(subSites.containsKey(ID))
		{
			return subSites.get(ID);
		}
		
		return null;
	}

	@Override
	public TSession CreateSession(TGlobParam parameter)
	{
		return sessionFactory.CreateSession(parameter);
	}
	
	public ISiteSession InitializeSubSiteSession(int ID, TSession siteSession, TGlobParam param)
	{
		if(!HasSubSite(ID))
		{
			return null;
		}
		
		ISiteSession subSiteSession = GetSubSite(ID).CreateSession(param);
		
		if(subSiteSession == null || !siteSession.SetSubSiteSession(ID, subSiteSession)) { return null; }
		
		return subSiteSession;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public final Boolean Process(ISiteSession caller, TGlobParam extData, TSession session)
	{
		if(session.HasActiveSubSiteSession())
		{
			if(!HasSubSite(session.GetActiveSubSiteSessionID()))
			{
				throw new NoSuchSiteException(this, session, session.GetActiveSubSiteSessionID());
			}
			
			ISiteSession subSiteSession = GetActiveSubSiteSession(session, extData);
			
			ISite<? super TGlobParam, ?> subSite = GetSubSite(session.GetActiveSubSiteSessionID());
			
			try
			{
				// SENSITIVE CODE
				return (Boolean) ((ISite)subSite).Process(session, extData, subSiteSession);
				// SENSITIVE CODE
			}
			catch(Throwable t)
			{
				Bukkit.getLogger().info("testThrowable?");
				
				t.printStackTrace();
			}
		}
		
		return ProcessThis(caller, extData, session);
	}
	
	public ISiteSession GetSubSiteSession(int ID, TSession siteSession, TGlobParam param)
	{
		if(siteSession.HasSubSiteSession(ID))
		{
			return siteSession.GetSubSiteSession(ID);
		}
		
		if(!HasSubSite(ID))
		{
			throw new NoSuchSiteException(this, siteSession, ID);
		}
		
		return InitializeSubSiteSession(ID, siteSession, param);
	}
	
	public ISiteSession GetActiveSubSiteSession(TSession siteSession, TGlobParam param)
	{
		if(siteSession.HasActiveSubSiteSession())
		{
			if(siteSession.HasSubSiteSession(siteSession.GetActiveSubSiteSessionID()))
			{
				return siteSession.GetActiveSubSiteSession();
			}
			
			return InitializeSubSiteSession(siteSession.GetActiveSubSiteSessionID(), siteSession, param);
		}
		
		return null;
	}
	
	public abstract boolean ProcessThis(ISiteSession caller, TGlobParam extData, TSession session);
}
