Under development

Processor-Session Library is a library at first designed for MagicCrafting plugin, when I was dealing with interactive menu implementation using inventory GUI.

At this stage this library is made by two major structures: processor and session.
Processor is an object that processes sessions and given data.
Session (or Caller) is an object that is usually temporal, which contains data specifically assigned to that session.
There is also a Data object, which carries data related to Processor call (especially data commonly used by every processor in cascade/recurrence Processor call, for example Inventory object).

Menu object contains a tree of MenuSite, which are the actual Processors. MenuSession object, analogically, contains a tree of MenuSiteSessions created for specific player when he is opening menu).
This kind of implementation allows for design of quite complicated and stable menus, which is done in MagicCrafting (although it still requires work due to debug code involved in).



Tested with Spigot-1.9.4-R0.1-SNAPSHOT

Email: michal.s.jagodzinski@gmail.com